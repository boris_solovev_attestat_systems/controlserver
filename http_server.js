var express = require('express');
var app = express();
var path = require('path');
var formidable = require('formidable');
const jwt = require('express-jwt');
const jwks = require('jwks-rsa');
const cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');
var server_config = require('./server_config');

app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use(bodyParser.json({limit: '150mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '150mb' }));
app.use(cors());

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.post('/upload', function(req, res){
  var form = new formidable.IncomingForm();

  form.multiples = false;
  form.uploadDir = path.join(__dirname, '/uploads');
  form.on('file', function(field, file) {
    try{
      const fileName = path.basename(file.path) + '.' + file.name.split('.').pop();
      fs.rename(file.path, path.join(form.uploadDir, fileName));
      form.content_file = file;
      form.fileName = fileName;
    } catch(err){ console.log('Error: ' + err)}
  });

  form.on('error', function(err) {
    console.log('An error has occured: \n' + err);
  });

  form.on('end', function() {
    console.log('upload completed: ' + form.content_file.name);
    const url = server_config.domain.proto + '://' + server_config.domain.host + ':' + server_config.domain.port + '/uploads/' + form.fileName;
    res.end(url);
  });

  form.parse(req);
});

var server = app.listen(server_config.http.port, function(){
  console.log('Server listening on port ' + server_config.http.port);
});

module.exports = server;