module.exports = function(sequelize, Sequelize) {
    var Vehicle = sequelize.define('vehicle', {
        id: {type: Sequelize.INTEGER, primaryKey: true, unique: true, autoIncrement: true},
        type : {type: Sequelize.TEXT},
        registration_number : {type: Sequelize.TEXT},
        route_number: {type: Sequelize.TEXT},
        contact: {type: Sequelize.TEXT},
        description: {type: Sequelize.TEXT}
    }, {
         timestamps: false,
         underscored: true,
         freezeTableName: true
    });

    Vehicle.getById = async(id) => {
        const vehicle = await Vehicle.findOne({where: {id}});

        return vehicle;
    }

    Vehicle.add = async(c, type, registration_number, route_number, contact, description) => {
        const result = await Vehicle.create({
                type,
                registration_number,
                route_number,
                contact,
                description
        });

        sequelize.userLog.log(c, 'addVehicle', JSON.stringify(result));

        return result;
    }

    Vehicle.set = async(c, id, type, registration_number, route_number, contact, description) => {
        const result = await Vehicle.update({
            type,
            registration_number,
            route_number,
            contact,
            description
        }, {where: {id}});

        sequelize.userLog.log(c, 'updateVehicle', JSON.stringify({id, type, registration_number, route_number, contact, description}));

        return result;
    }

    Vehicle.remove = async(c, id) => {
        const result = await Vehicle.destroy({where : { id  }});

        sequelize.userLog.log(c, 'removeVehicle', JSON.stringify({id}));

        return result;
    }

    Vehicle.getList = async() => {
        const vehicles = await sequelize.query(
            'SELECT *, public."vehicle".id as id, public."terminal".id as terminal_id FROM public."vehicle" LEFT OUTER JOIN public."terminal" ON (public."terminal".vehicle_id = public."vehicle".id)',
            { type: sequelize.QueryTypes.SELECT}
        );

        return vehicles;
    }

    function test() {
        /*Vehicle.add(1, 'type2', 'registration_number2', 'route_number2', 'contact2', 'description2').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*Vehicle.set(1, 1, 'type3', 'registration_number3', 'route_number3', 'contact3', 'description3').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*Vehicle.remove(1, 2).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*Vehicle.getById(1).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        Vehicle.getList().then((r) => {
            console.log(JSON.stringify(r));
        });
    }

    //test();

    return Vehicle;
}
