module.exports = function(sequelize, Sequelize) {
    var Terminal = sequelize.define('terminal', {
        id: {type: Sequelize.INTEGER, primaryKey: true, unique: true, autoIncrement: true},
        serial : {type: Sequelize.TEXT},
        description : {type: Sequelize.TEXT},
        vehicle_id: {type: Sequelize.INTEGER},
        last_activity: {type: Sequelize.DATE},
    }, {
         timestamps: false,
         underscored: true,
         freezeTableName: true
    });

    Terminal.getById = async(id) => {
        const terminal = await Terminal.findOne({where: {id}});

        return terminal;
    }

    Terminal.getByVehicleId = async(vehicle_id) => {
        const terminal = await Terminal.findAll({where: {vehicle_id}});

        return terminal;
    }

    Terminal.getBySerial = async(serial) => {
        const terminal = await Terminal.findOne({where: {serial}});

        return terminal;
    }

    Terminal.add = async(c, serial, description, vehicle_id) => {
        const result = await Terminal.create({
            serial, description, vehicle_id
        });

        sequelize.userLog.log(c, 'addTerminal', JSON.stringify(result));

        return result;
    }

    Terminal.set = async(c, id, serial, description, vehicle_id) => {
        const result = await Terminal.update({
            serial, description, vehicle_id
        }, {where: {id}});

        sequelize.userLog.log(c, 'updateTerminal', JSON.stringify({id, serial, description, vehicle_id}));

        return result;
    }

    Terminal.remove = async(c, id) => {
        const result = await Terminal.destroy({where : { id  }});

        sequelize.userLog.log(c, 'removeTerminal', JSON.stringify({id}));

        return result;
    }

    Terminal.getList = async() => {
        const terminals = await sequelize.query(
            'SELECT *, public."terminal".id as id, public."vehicle".id as vehicle_id FROM public."terminal" LEFT OUTER JOIN public."vehicle" ON (public."terminal".vehicle_id = public."vehicle".id)',
            { type: sequelize.QueryTypes.SELECT}
        );

        return terminals;
    }

    Terminal.checkTerminal = async (serial, secret) => {
        const terminals = await sequelize.query('SELECT * FROM public."terminal" WHERE serial=\'' + serial + '\'', { type: sequelize.QueryTypes.SELECT});

        if(!terminals || !terminals.length) return null;

        return terminals[0];
    }
    
    function test() {
        /*Terminal.add(1, 'serial', 'description', 1, 'model', 'software').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*Terminal.add(1, 'serial2', 'description2', 1, 'model2', 'software2').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*Terminal.set(1, 1, 'serial3', 'description3', 1, 'model3', 'software3').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*Terminal.remove(1, 3).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*Terminal.getById(1).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*Terminal.getList().then((r) => {
            console.log(JSON.stringify(r));
        });*/
    }

    //test();

    return Terminal;
}
