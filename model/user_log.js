module.exports = function(sequelize, Sequelize) {
    var UserLog = sequelize.define('user_log', {
        log_time : {type: Sequelize.DATE},
        user_id : {type: Sequelize.INTEGER},
        action: {type: Sequelize.TEXT},
        params: {type: Sequelize.TEXT}
    }, {
         timestamps: false,
         underscored: true,
         freezeTableName: true
    });

    UserLog.log = async(userId, action, params) => {
        if(!params) params = 'NULL';

        return await sequelize.query(
            "INSERT INTO user_log(log_time, user_id, action, params) \
            VALUES (current_timestamp, :user_id, :action, :params);",
            { 
                replacements: {
                    user_id: userId,
                    action,
                    params
                },
                type: sequelize.QueryTypes.INSERT
            });
    }

/*    function test() {
        UserLog.log(1, 'test', '{params: []}').then((r) => {
            console.log(JSON.stringify(r));
        });
    }

    test();*/

    return UserLog;
}
