var Sequelize = require('sequelize');
var path = require("path");
var server_config = require('../server_config');
var async = require('async');

var sequelize = new Sequelize(server_config.database.database, server_config.database.user, server_config.database.password, {
    host: server_config.database.address,
    port: server_config.database.port,
    dialect: 'postgres'
});

var userLog = sequelize.import(path.join(__dirname, 'user_log.js'));

sequelize.userLog = userLog;

sequelize.VALUE_NAMES = [
    'serial',               // 0
    'in_count',             // 1
    'out_count',            // 2
    'storage_size',	    //3
    'storage_total',	    // 4
    'storage_used',	    // 5
    'startStorageLimit',	    //6
    'stopStorageLimit',	    //7
    'syncTime',	    //8
    'software',	    //9
    'active',	    //10
    'model',	    //11
    'lowLinePos',	    //12
    'highLinePos',	    //13
    'objectSizeLimit',	    //14
    'recordVideo',	    //15
    'recordDepth',	    //16
    'doAnalyze',	    //17
    'index',	    //18
    'door',	    //19
    'lowThreshold',	    //20
    'highThreshold',	    //21
    'erosion_size',	    //22
    'sNear',	    //23
    'maxObjectFrames',	    //24
    'restart',	    //25
    'message',	    //26
    'ping',	    //27
    'uploadServerUrl',	    //28
    'reverse', // 29,
    'recordStart', // 30
    'recordDone', // 31
    'uploadStart', // 32
    'uploadDone', // 33
    'uploadError', // 34
    'temp', // 35,
    'startDiskLimitAlarm', // 36
    'startDiskLimitRecovery', // 37
    'stopDiskLimitAlarm',
    'stopDiskLimitRecovery'
];

sequelize.KEEP_HISTORY_FOR = [
    'restart',
    'in_count',
    'out_count',
    'message',
    'storage_size',
    'storage_used',
    'recordStart',
    'recordDone',
    'uploadStart',
    'uploadDone',
    'uploadError',
    'temp',
    'startDiskLimitAlarm', // 36
    'startDiskLimitRecovery', // 37
    'stopDiskLimitAlarm',
    'stopDiskLimitRecovery'
];

sequelize.subscriptions = [
    {event: 'restart', peer: 'boris.solovev@attestat.systems'},
    {event: 'startDiskLimitAlarm', peer: 'boris.solovev@attestat.systems'},
    {event: 'startDiskLimitRecovery', peer: 'boris.solovev@attestat.systems'},
    {event: 'stopDiskLimitAlarm', peer: 'boris.solovev@attestat.systems'},
    {event: 'stopDiskLimitRecovery', peer: 'boris.solovev@attestat.systems'},

    {event: 'restart', peer: 'ilyayaroshenko@attestat.systems'},
    {event: 'startDiskLimitAlarm', peer: 'ilyayaroshenko@attestat.systems'},
    {event: 'startDiskLimitRecovery', peer: 'ilyayaroshenko@attestat.systems'},
    {event: 'stopDiskLimitAlarm', peer: 'ilyayaroshenko@attestat.systems'},
    {event: 'stopDiskLimitRecovery', peer: 'ilyayaroshenko@attestat.systems'}
]


var user = sequelize.import(path.join(__dirname, 'user.js'));
var vehicle = sequelize.import(path.join(__dirname, 'vehicle.js'));
var terminal = sequelize.import(path.join(__dirname, 'terminal.js'));
var terminalChannel = sequelize.import(path.join(__dirname, 'terminal_channel.js'));
var terminalState = sequelize.import(path.join(__dirname, 'terminal_state.js'));
var channelState = sequelize.import(path.join(__dirname, 'channel_state.js'));
var terminalData = sequelize.import(path.join(__dirname, 'terminal_data.js'));
terminalData.terminalState = terminalState;
terminalData.channelState = channelState;
terminal.state = terminalState;
terminalChannel.state = channelState;
var fileState = sequelize.import(path.join(__dirname, 'file_state.js'));

module.exports = {
    userLog,
    user,
    vehicle,
    terminal,
    terminalChannel,
    terminalState,
    channelState,
    terminalData,
    fileState,
    sequelize : sequelize
}
