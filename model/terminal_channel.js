module.exports = function(sequelize, Sequelize) {
    var TerminalChannel = sequelize.define('terminal_channel', {
        id: {type: Sequelize.INTEGER, primaryKey: true, unique: true, autoIncrement: true},
        terminal_id: {type: Sequelize.INTEGER},
        serial : {type: Sequelize.TEXT},
        door : {type: Sequelize.TEXT},
        description: {type: Sequelize.TEXT}
    }, {
         timestamps: false,
         underscored: true,
         freezeTableName: true
    });

    TerminalChannel.getById = async(id) => {
        const terminalChannel = await TerminalChannel.findOne({where: {id}});

        return terminalChannel;
    }

    TerminalChannel.getByTerminalAndSerial = async(terminal_id, serial) => {
        const terminalChannel = await TerminalChannel.findOne({where: {terminal_id, serial}});

        return terminalChannel;
    }

    TerminalChannel.getBySerial = async(serial) => {
        const terminalChannel = await TerminalChannel.findOne({where: {serial}});

        return terminalChannel;
    }

    TerminalChannel.add = async(c, terminal_id, serial, door, description) => {
        const result = await TerminalChannel.create({
            terminal_id, serial, door, description
        });

        if(c) sequelize.userLog.log(c, 'addTerminalChannel', JSON.stringify(result));

        return result;
    }

    TerminalChannel.set = async(c, id, serial, door, description) => {
        const result = await TerminalChannel.update({
            serial, door, description
        }, {where: {id}});

        sequelize.userLog.log(c, 'updateTerminalChannel', JSON.stringify({id, serial, door, description}));

        return result;
    }

    TerminalChannel.remove = async(c, id) => {
        const result = await TerminalChannel.destroy({where : { id  }});

        sequelize.userLog.log(c, 'removeTerminalChannel', JSON.stringify({id}));

        return result;
    }

    TerminalChannel.getList = async() => {
        const terminalChannels = await TerminalChannel.findAll();

        return terminalChannels;
    }

    TerminalChannel.getListByTerminal = async(terminal_id) => {
        const terminalChannels = await TerminalChannel.findAll({where: {terminal_id}});

        return terminalChannels;
    }

    function test() {
        /*TerminalChannel.add(3, 6, 'type2', 'registration_number2', 'route_number2', 'contact2', 'description2').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*TerminalChannel.set(1, 1, 'type3', 'registration_number3', 'route_number3', 'contact3', 'description3').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*TerminalChannel.remove(1, 2).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*TerminalChannel.getById(1).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*TerminalChannel.getList().then((r) => {
            console.log(JSON.stringify(r));
        });*/
    }

    //test();

    return TerminalChannel;
}
