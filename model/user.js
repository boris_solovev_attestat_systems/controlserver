module.exports = function(sequelize, Sequelize) {
    var User = sequelize.define('user', {
        name : {type: Sequelize.TEXT},
        password : {type: Sequelize.TEXT},
        description: {type: Sequelize.INTEGER},
        role: {type: Sequelize.INTEGER}
    }, {
         timestamps: false,
         underscored: true,
         freezeTableName: true
    });

    User.getByName = async(name) => {
        const users = await sequelize.query('SELECT * FROM public."user" WHERE name=\'' + name + '\'', { type: sequelize.QueryTypes.SELECT});

        if(users && users.length > 0) {
            delete users[0].password;
            return users[0];
        }
        return null;
    }

    User.getById = async(id) => {
        const users = await sequelize.query('SELECT * FROM public."user" WHERE id=' + id, { type: sequelize.QueryTypes.SELECT});

        if(users && users.length > 0) {
            delete users[0].password;
            return users[0];
        }
        return null;
    }

    User.add = async(c, name, password, description, role) => {
        const result = (await sequelize.query(
            'INSERT INTO public."user"(name, password, description, role) \
            VALUES (:name, :password, :description, :role) \
            ; SELECT currval(\'"user_id_seq"\'::regclass);',
            { 
                replacements: {
                    name,
                    password,
                    description,
                    role
                },
                type: sequelize.QueryTypes.SELECT
            }))[0].currval;

        sequelize.userLog.log(c, 'addUser', JSON.stringify({id: result, name, description, role}));

        return result;
    }

    User.update = async(c, id, name, description, role) => {
        const result = (await sequelize.query(
            'UPDATE public."user" SET name=:name,description=:description, role=:role WHERE id=:id',
            {
                replacements: {
                    id,
                    name,
                    description,
                    role
                },
                type: sequelize.QueryTypes.UPDATE
            }
        ))[1];

        sequelize.userLog.log(c, 'updateUser', JSON.stringify({id, name, description, role}));

        return result;
    }

    User.setPassword = async(c, id, password) => {
        const result = (await sequelize.query(
            'UPDATE public."user" SET password=\'' + password + '\' WHERE id=' + id,
            {
                type: sequelize.QueryTypes.UPDATE
            }
        ))[1];

        sequelize.userLog.log(c, 'setUserPassword', JSON.stringify({id}));

        return result;
    }

    User.remove = async(c, _id) => {
        const result = await User.destroy({where : { id : _id }});

        sequelize.userLog.log(c, 'removeUser', JSON.stringify({_id}));

        return result;
    }

    User.getList = async() => {
        const users = await sequelize.query('SELECT * FROM public."user" ', { type: sequelize.QueryTypes.SELECT});

        users.forEach(element => {
            delete element.password;
        });

        return users;
    }

    User.checkUser = async (name, password) => {
        const users = await sequelize.query('SELECT * FROM public."user" WHERE name=\'' + name + '\' AND password=\'' + password + '\'', { type: sequelize.QueryTypes.SELECT});

        if(!users || !users.length) return null;

        return users[0];
    }

    function test() {
        /*User.add(1, 'N', 'P', 'D', 1).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*User.update(1, 8, 'n1', 'd1', 1).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*User.setPassword(1, 8, 'p1').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*User.remove(1, 10).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*User.getById(8).then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*User.getByName('n1').then((r) => {
            console.log(JSON.stringify(r));
        });*/

        /*User.getList().then((r) => {
            console.log(JSON.stringify(r));
        });*/
    }

    test();

    return User;
}
