module.exports = function(sequelize, Sequelize) {
    var TerminalData = sequelize.define('terminal_data', {
        id: {type: Sequelize.INTEGER, primaryKey: true, unique: true, autoIncrement: true},
        terminal_id: {type: Sequelize.INTEGER},
        channel_id: {type: Sequelize.INTEGER},
        value: {type: Sequelize.INTEGER},
        r_ts: {type: Sequelize.DATE},
        g_ts: {type: Sequelize.DATE},
        int_data: {type: Sequelize.INTEGER},
        text_data: {type: Sequelize.TEXT},
    }, {
         timestamps: false,
         underscored: true,
         freezeTableName: true
    });

    TerminalData.getByTerminalId = async(terminal_id, startDate, stopDate) => {
        const terminalData = await TerminalData.findAll(
            {
                where: 
                {
                    terminal_id,
                    g_ts: {
                        [sequelize.Op.gt]: startDate,
                        [sequelize.Op.lt]: stopDate
                    }
                }
            });

        return terminalData;
    }

    TerminalData.getByChannelId = async(channel_id, value, startDate, stopDate) => {
        const terminalData = await TerminalData.findAll(
            {
                where: 
                {
                    channel_id,
                    value,
                    g_ts: {
                        [sequelize.Op.gt]: startDate,
                        [sequelize.Op.lt]: stopDate
                    }
                }
            });

        return terminalData;
    }

    TerminalData.add = async(terminal_id, channel_id, value, g_ts, data, keep_history) => {
        let intValue = parseInt(data);
        if(!intValue) intValue = 0;
        const result = await TerminalData.create({
            terminal_id, channel_id, value, g_ts, r_ts: new Date(), int_data: intValue, text_data: JSON.stringify(data)
        });

        if(channel_id) await TerminalData.channelState.set(channel_id, value, result.id);
        else await TerminalData.terminalState.set(terminal_id, value, result.id);

        if(!keep_history) {
            await TerminalData.destroy({where: {id: {$ne: result.id}, terminal_id, channel_id, value}});
        }

        await sequelize.query("UPDATE public.terminal SET last_activity=current_timestamp WHERE id=" + terminal_id);

        return result;
    }

    /*function test() {
        const t = 30;
        const c = 20;

        TerminalData.add(t, null, sequelize.VALUE_NAMES.indexOf('storage_size'), new Date(), 320 * 1024).then(() => {});
        TerminalData.add(t, null, sequelize.VALUE_NAMES.indexOf('storage_used'), new Date(), 1024).then(() => {});
        TerminalData.add(t, null, sequelize.VALUE_NAMES.indexOf('storage_start_limit'), new Date(), 1024).then(() => {});
        TerminalData.add(t, null, sequelize.VALUE_NAMES.indexOf('storage_stop_limit'), new Date(), 1024 * 20).then(() => {});
        TerminalData.add(t, null, sequelize.VALUE_NAMES.indexOf('sync_time'), new Date(), 60).then(() => {});
    
        TerminalData.add(t, c, sequelize.VALUE_NAMES.indexOf('in_count'), new Date(), 1).then(() => {});
        TerminalData.add(t, c, sequelize.VALUE_NAMES.indexOf('out_count'), new Date(), 2).then(() => {});
        TerminalData.add(t, c, sequelize.VALUE_NAMES.indexOf('low_line_pos'), new Date(), 50).then(() => {});
        TerminalData.add(t, c, sequelize.VALUE_NAMES.indexOf('hight_line_pos'), new Date(), 400).then(() => {});
        TerminalData.add(t, c, sequelize.VALUE_NAMES.indexOf('detect_size'), new Date(), 400).then(() => {});
        TerminalData.add(t, c, sequelize.VALUE_NAMES.indexOf('write_video'), new Date(), 1).then(() => {});
        TerminalData.add(t, c, sequelize.VALUE_NAMES.indexOf('write_depth'), new Date(), 0).then(() => {});
        TerminalData.add(t, c, sequelize.VALUE_NAMES.indexOf('analyze'), new Date(), 1).then(() => {});
    }

    test();*/

    return TerminalData;
}
