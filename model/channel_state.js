module.exports = function(sequelize, Sequelize) {
    var ChannelState = sequelize.define('channel_state', {
        channel_id: {type: Sequelize.INTEGER, primaryKey: true, unique: true},
        value : {type: Sequelize.INTEGER},
        last_id : {type: Sequelize.INTEGER}
        },
        {
         timestamps: false,
         underscored: true,
         freezeTableName: true
        }
    );

    ChannelState.getByChannelId = async(channel_id) => {
        const channelState = await ChannelState.findAll({where: {channel_id}});

        const result = {};

        for(let i = 0; i < channelState.length; i++) {
            const element = channelState[i];
            const r = await sequelize.query('SELECT * FROM public."terminal_data" WHERE id=' + element.last_id,
            { type: sequelize.QueryTypes.SELECT});

            if(r.length) {
                r[0].int_data = parseInt(r[0].int_data);
                result[sequelize.VALUE_NAMES[element.value]] = r[0];
            }
        };

        return result;
    }

    ChannelState.set = async(channel_id, value, last_id) => {
        await sequelize.query('DELETE FROM public."channel_state" WHERE channel_id=' + channel_id + ' AND value=' + value + '; \
        INSERT INTO public."channel_state" (channel_id, value, last_id) VALUES (' + channel_id + ', ' + value + ', ' + last_id + ');');
    }

    function test() {
        /*ChannelState.getByChannelId(19).then((r) => {
            console.log(JSON.stringify(r, null, ' '));
        });*/
    }

    //test();

    return ChannelState;
}
