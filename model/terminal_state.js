module.exports = function(sequelize, Sequelize) {
    var TerminalState = sequelize.define('terminal_state', {
        terminal_id: {type: Sequelize.INTEGER, primaryKey: true, unique: true},
        value : {type: Sequelize.INTEGER},
        last_id : {type: Sequelize.INTEGER}
        },
        {
         timestamps: false,
         underscored: true,
         freezeTableName: true
        }
    );

    TerminalState.getByTerminalId = async(terminal_id) => {
        const terminalState = await TerminalState.findAll({where: {terminal_id}});

        const result = {};

        for(let i = 0; i < terminalState.length; i++) {
            const element = terminalState[i];
            const r = await sequelize.query('SELECT * FROM public."terminal_data" WHERE id=' + element.last_id,
            { type: sequelize.QueryTypes.SELECT});

            if(r.length) {
                r[0].int_data = parseInt(r[0].int_data);
                result[sequelize.VALUE_NAMES[element.value]] = r[0];
            }
        };

        return result;
    }

    TerminalState.set = async(terminal_id, value, last_id) => {
        await sequelize.query('DELETE FROM public."terminal_state" WHERE terminal_id=' + terminal_id + ' AND value=' + value + '; \
        INSERT INTO public."terminal_state" (terminal_id, value, last_id) VALUES (' + terminal_id + ', ' + value + ', ' + last_id + ');');
    }

    function test() {
        /*TerminalState.getByTerminalId(28).then((r) => {
            console.log(JSON.stringify(r, null, ' '));
        });*/
    }

    //test();

    return TerminalState;
}
