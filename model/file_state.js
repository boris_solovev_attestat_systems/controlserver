module.exports = function(sequelize, Sequelize) {
    var FileState = sequelize.define('file_state', {
        id: {type: Sequelize.INTEGER, primaryKey: true, unique: true, autoIncrement: true},
        terminal_id: {type: Sequelize.INTEGER},
        channel_id: {type: Sequelize.INTEGER},
        type: {type: Sequelize.TEXT},
        name : {type: Sequelize.TEXT},
        state: {type: Sequelize.TEXT},
        createTime: {type: Sequelize.DATE},
        uploadTime: {type: Sequelize.DATE},
        size: {type: Sequelize.INTEGER}
    }, {
         timestamps: false,
         underscored: true,
         freezeTableName: true
    });

    FileState.getById = async(id) => {
        const fileState = await FileState.findOne({where: {id}});

        return fileState;
    }

    FileState.getByTerminalId = async(terminal_id) => {
        const fileState = await FileState.findOne({where: {terminal_id}});

        return fileState;
    }

    FileState.getByChannelId = async(channel_id) => {
        const fileState = await FileState.findOne({where: {chennel_id}});

        return fileState;
    }

    FileState.set = async(terminal_id, channel_id, type, name, state, size) => {
        let s = await FileState.findOne({
            where: {
                terminal_id,
                channel_id,
                type,
                name: {$iLike: name.toLowerCase()}
            }
        });

        if(!s) {
            s = new FileState();
            s.terminal_id = terminal_id;
            s.channel_id = channel_id;
            s.type = type;
            s.name = name;
        }

        s.state = state;
        if(size) s.size = size;

        switch(state) {
            case 'recordStart':
                s.createTime = new Date();
                break;
            case 'recordDone':
                break;
            case 'uploadStart':
                break;
            case 'uploadDone':
                s.uploadTime = new Date();
                break;
        }

        s.save();

        return s;
    }

    FileState.getList = async() => {
        const terminalChannels = await FileState.findAll();

        return terminalChannels;
    }

    FileState.getListByTerminal = async(terminal_id) => {
        const terminalChannels = await FileState.findAll({where: {terminal_id}});

        return terminalChannels;
    }

    function test() {
        //FileState.sync({ force: true });

        FileState.set(1, 2, 'type', 'NamE1', 'analyzeFinish', 'p2').then((s) => {
            console.log('s: ' + JSON.stringify(s));
        });
    }

    //test();

    return FileState;
}
