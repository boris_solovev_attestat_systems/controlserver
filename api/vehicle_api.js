module.exports = function(model, api) {
    api.interface['vehicle'] = {};
    api.control['vehicle'] = {};

//
//                          Control
//
    async function defaultUser(connection, id, args) {
        if(connection.userId == undefined) return 'Не авторизованный доступ';

        return null;
    }

    async function adminUser(connection, id, args) {
        if(connection.userId == undefined) return 'Не авторизованный доступ';
        if(!connection.user.role == 1) return 'Отказано в доступе';

        return null;
    }

    api.control['vehicle']['get'] = defaultUser;

    api.control['vehicle']['getList'] = defaultUser;

    api.control['vehicle']['add'] = adminUser;

    api.control['vehicle']['update'] = adminUser;

    api.control['vehicle']['remove'] = adminUser;

//
//                          API
//

    api.interface['vehicle']['get'] = async(connection, id, args) => {
        const vehicle = await model.vehicle.getById(args.id);
        if(vehicle == null) {
            api.error(connection, id, {error: 'ТС не существует'});
            return;
        }

        api.answer(connection, id, vehicle);
    }

    api.interface['vehicle']['getList'] = async(connection, id, args) => {
        const vehicleList = await model.vehicle.getList();
        api.answer(connection, id, vehicleList);
    }

    api.interface['vehicle']['add'] = async (connection, id, args) => {
        const vehicle = await model.vehicle.add(
            connection.userId,
            args.type, 
            args.registration_number,
            args.route_number,
            args.contact,
            args.description
        );

        api.answer(connection, id, {id: vehicle.id});
    }

    api.interface['vehicle']['update'] = async (connection, id, args) => {
        const rows = model.vehicle.set(
            connection.userId,
            args.id,
            args.type, 
            args.registration_number,
            args.route_number,
            args.contact,
            args.description
        );

        api.answer(connection, id, rows);
    }

    api.interface['vehicle']['remove'] = async (connection, id, args) => {
        await model.vehicle.remove(connection.userId, args.id);

        api.answer(connection, id, {result : "ok"});
    }
}
