module.exports = function(model, api) {
    api.interface['terminal'] = {};
    api.control['terminal'] = {};

//
//                          Control
//
    async function defaultUser(connection, id, args) {
        if(connection.userId == undefined) return 'Не авторизованный доступ';

        return null;
    }

    async function adminUser(connection, id, args) {
        if(connection.userId == undefined) return 'Не авторизованный доступ';
        if(!connection.user.role == 1) return 'Отказано в доступе';

        return null;
    }

    api.control['terminal']['get'] = defaultUser;
    api.control['terminal']['getState'] = defaultUser;
    api.control['terminal']['getList'] = defaultUser;
    api.control['terminal']['getListByVehicle'] = defaultUser;
    api.control['terminal']['add'] = adminUser;
    api.control['terminal']['update'] = adminUser;
    api.control['terminal']['remove'] = adminUser;

    api.control['terminal']['getChannel'] = defaultUser;
    api.control['terminal']['getChannelList'] = defaultUser;
    api.control['terminal']['addChannel'] = adminUser;
    api.control['terminal']['updateChannel'] = adminUser;
    api.control['terminal']['removeChannel'] = adminUser;
    api.control['terminal']['getChannelState'] = defaultUser;
    api.control['terminal']['getChannelData'] = defaultUser;
    api.control['terminal']['getTerminalData'] = defaultUser;
    api.control['terminal']['installSoftware'] = defaultUser;

//
//                          API
//

    api.interface['terminal']['get'] = async(connection, id, args) => {
        const terminal = await model.terminal.getById(args.id);
        if(terminal == null) {
            api.error(connection, id, {error: 'ТС не существует'});
            return;
        }

        api.answer(connection, id, terminal);
    }

    api.interface['terminal']['getList'] = async(connection, id, args) => {
        const terminalList = await model.terminal.getList();
        api.answer(connection, id, terminalList);
    }

    api.interface['terminal']['add'] = async (connection, id, args) => {
        let terminal = await model.terminal.getBySerial(args.serial);
        if(terminal) throw new Error('Терминал с таким серийным номером существует');

        terminal = await model.terminal.add(
            connection.userId,
            args.serial, args.description, args.vehicle_id, args.model, args.software
        );

        api.answer(connection, id, {id: terminal.id});
    }

    api.interface['terminal']['update'] = async (connection, id, args) => {
        let terminal = await model.terminal.getBySerial(args.serial);
        if(terminal && terminal.id != args.id) throw new Error('Терминал с таким серийным номером существует');

        const rows = await model.terminal.set(
            connection.userId,
            args.id, args.serial, args.description, args.vehicle_id, args.model, args.software
        );

        api.answer(connection, id, rows);
    }

    api.interface['terminal']['remove'] = async (connection, id, args) => {
        await model.terminal.remove(connection.userId, args.id);

        api.answer(connection, id, {result : "ok"});
    }

    api.interface['terminal']['getChannel'] = async(connection, id, args) => {
        const channel = await model.terminalChannel.getById(args.id);
        if(channel == null) {
            api.error(connection, id, {error: 'Канал не существует'});
            return;
        }

        api.answer(connection, id, channel);
    }

    api.interface['terminal']['getChannelList'] = async(connection, id, args) => {
        const channelList = await model.terminalChannel.getListByTerminal(args.terminal_id);
        api.answer(connection, id, channelList);
    }

    api.interface['terminal']['addChannel'] = async (connection, id, args) => {
        let channel = await model.terminalChannel.getBySerial(args.serial);
        if(channel) throw new Error('Канал с таким серийным номером существует');

        channel = await model.terminalChannel.add(
            connection.userId,
            args.terminal_id, args.serial, args.door, args.description
        );

        api.answer(connection, id, {id: channel.id});
    }

    api.interface['terminal']['updateChannel'] = async (connection, id, args) => {
        let channel = await model.terminalChannel.getBySerial(args.serial);
        if(channel && channel.id != args.id) throw new Error('Канал с таким серийным номером существует');

        const rows = await model.terminalChannel.set(
            connection.userId,
            args.id, args.serial, args.door, args.description
        );

        api.answer(connection, id, rows);
    }

    api.interface['terminal']['removeChannel'] = async (connection, id, args) => {
        await model.terminalChannel.remove(connection.userId, args.id);

        api.answer(connection, id, {result : "ok"});
    }

    api.interface['terminal']['getListByVehicle'] = async (connection, id, args) => {
        const rows = await model.terminal.getByVehicleId(args.vehicle_id);

        api.answer(connection, id, rows);
    }

    api.interface['terminal']['getState'] = async (connection, id, args) => {
        const rows = await model.terminal.state.getByTerminalId(args.id);

        api.answer(connection, id, rows);
    }

    api.interface['terminal']['getChannelState'] = async (connection, id, args) => {
        const rows = await model.terminalChannel.state.getByChannelId(args.id);

        api.answer(connection, id, rows);
    }

    api.interface['terminal']['getChannelData'] = async (connection, id, args) => {
        const rows = await model.terminalData.getByChannelId(
            parseInt(args.channel_id),
            model.sequelize.VALUE_NAMES.indexOf(args.value),
            new Date(args.startDate),
            new Date(args.stopDate));

        api.answer(connection, id, rows);
    }

    api.interface['terminal']['getTerminalData'] = async (connection, id, args) => {
        const rows = await model.terminalData.getByTerminalId(
            parseInt(args.terminal_id),
            new Date(args.startDate),
            new Date(args.stopDate));

        const result = [];

        const channelIdxChache = {};
        for(let i = 0; i < rows.length; i++){

            if(rows[i].channel_id) {
                if(!channelIdxChache[rows[i].channel_id]) {
                    channelIdxChache[rows[i].channel_id] = (await model.terminalChannel.getById(rows[i].channel_id)).serial;
                }

                rows[i].channel_serial = channelIdxChache[rows[i].channel_id];
            }
            
            rows[i].value_name = model.sequelize.VALUE_NAMES[rows[i].value];

            const newRow = {
                g_ts: rows[i].g_ts,
                channel_serial: rows[i].channel_serial,
                value_name: rows[i].value_name,
                int_data: rows[i].int_data,
                text_data: rows[i].text_data
            }
            result.push(newRow);
        }

        api.answer(connection, id, result);
    }
}
