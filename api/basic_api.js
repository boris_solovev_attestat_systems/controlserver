var server_config = require('./../server_config');
var uuid = require('node-uuid');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var timestamp = require('unix-timestamp');

module.exports = function(model, api) {
    api.interface['basic'] = {};
    api.control['basic'] = {};

//
//                                  Control
//

/*
                Control template

    api.control[][] = function(connection, id, args) {
        var promise = new Promise(function(resolve, reject){
            if(connection.userId == undefined) {
                resolve('Не авторизованный доступ');
                return;
            }

            resolve(null);
        });

        return promise;
    }
                

*/

    api.control.checkRegistered = async(connection, id, args) => {
        try {
            if(args.token) {
                const decoded = jwt.verify(args.token, server_config.ws.secret);

                if(decoded) {
                    connection.userId = parseInt(decoded.user);
                }
            }
        } catch(err) {
            console.log('checkRegistered: ' + err);
        }

        if(connection.userId == undefined)  return 'Не авторизованный доступ';

        if(connection.user == undefined) {
            user = await model.user.getById(connection.userId)
            if(user) connection.user = user;
            else return 'Не авторизованный доступ';
        }

        return null;
    }

    api.control['basic']['login'] = async(connection, id, args) => {
        if(connection.userId != undefined)  return 'Уже зарегистрирован';
        return null;
    }

    api.control['basic']['register'] = async(connection, id, args) => {
        //if(connection.terminal_id != undefined)  return 'Уже зарегистрирован';
        return null;
    }

    api.control['basic']['logout'] = async(connection, id, args) => {
        return null;
    }
    
    api.control['basic']['whoAmI'] = api.control.checkRegistered;

//
//                                  API
//

    api.interface['basic']['login'] = function(connection, id, args) {
        api.interface['basic'].checkUser(args.login, args.password).then(function(user){
            if(user == null) {
                api.error(connection, id, {error: 'Не верные учетные данные'});
                return;
            } 

            connection.userId = user.id;
            connection.user = user;
            const token = jwt.sign({user: user.id}, server_config.ws.secret, { expiresIn : 15 * 24 * 60 * 60 * 1000 });
            delete user.password;
            api.answer(connection, id, {result: 'ok', user, token});
        });
    }

    api.interface['basic']['register'] = function(connection, id, args) {
        api.interface['basic'].checkTerminal(args.serial, args.secret).then(function(terminal){
            if(terminal == null) {
                api.error(connection, id, {error: 'Не верные учетные данные'});
                return;
            } 

            connection.terminal_id = terminal.id;

            api.terminalConnections[terminal.id] = connection;

            const token = jwt.sign({terminal: terminal.id}, server_config.ws.secret, { expiresIn : 15 * 24 * 60 * 60 * 1000 });
            //delete terminal.secret;
            api.answer(connection, id, {result: 'ok', terminal, token, time: timestamp.now()});
        });
    }

    api.interface['basic']['logout'] = function(connection, id, args) {
        if(connection.userId != undefined) {
            delete connection.userId;
        }

        api.answer(connection, id, {result: 'ok'});
    }

    api.interface['basic']['whoAmI'] = function(connection, id, args) {
        model.user.getById(connection.userId)
        .then((user) => {
            delete user.password;
            api.answer(connection, id, {user:user});
        })
        .catch((err) => {
            api.error(connection, id, {error: err});
        });
    }
}