var nodemailer = require('nodemailer');

module.exports = function(model, api) {
    api.interface['control'] = {};
    api.control['control'] = {};

//
//                          Control
//
    async function defaultUser(connection, id, args) {
        if(connection.userId == undefined) return 'Не авторизованный доступ';

        return null;
    }

    async function adminUser(connection, id, args) {
        if(connection.userId == undefined) return 'Не авторизованный доступ';
        if(!connection.user.role == 1) return 'Отказано в доступе';

        return null;
    }

    async function defaultTerminal(connection, id, args) {
        if(connection.terminal_id == undefined) return 'Не авторизованный доступ';

        return null;
    }

    api.control['control']['terminalEvent'] = defaultTerminal;
    api.control['control']['terminalInfo'] = defaultTerminal;
    api.control['control']['ping'] = defaultTerminal;

    api.control['control']['fileAnalyzeStart'] = defaultTerminal;
    api.control['control']['fileAnalyzeStop'] = defaultTerminal;

    api.control['control']['subscribe'] = defaultUser;
    api.control['control']['unsubscribe'] = defaultUser;
    api.control['control']['unsubscribeAll'] = defaultUser;
    api.control['control']['subscribeStream'] = defaultUser;
    api.control['control']['unsubscribeStream'] = defaultUser;

    api.control['control']['installSoftware'] = adminUser;
    api.control['control']['resetCounters'] = adminUser;
    api.control['control']['readInfo'] = defaultUser;
    api.control['control']['setTerminalValue'] = adminUser;
    api.control['control']['setChannelValue'] = adminUser;

//
//                          API
//

    api.interface['control']['installSoftware'] = async(connection, id, args) => {
        if(!api.terminalConnections[args.terminal_id]) throw new Error("Нет соединения с терминалом");

        const result = await api.callAsync(api.terminalConnections[args.terminal_id], 'control', 'installSoftware', args);
        api.answer(connection, id, result);
    }

    api.interface['control']['terminalEvent'] = async(connection, id, args) => {
        if(args.report) {
            for(let i = 0; i < args.report.length; i++) {
                const evt = args.report[i];

                evt.time = new Date(evt.time);

                let channelId = null;
                if(evt.channel) {
                    const channel = await model.terminalChannel.getByTerminalAndSerial(connection.terminal_id, evt.channel);
                    channelId = channel.id;
                }

                const vid = model.sequelize.VALUE_NAMES.indexOf(evt.type);
                if(vid == -1) continue;

                await model.terminalData.add(connection.terminal_id, channelId, vid, evt.time, evt.data, model.sequelize.KEEP_HISTORY_FOR.indexOf(evt.type) != -1);

                switch(evt.type) {
                    case 'recordStart':
                        model.fileState.set(
                            connection.terminal_id,
                            channelId, 
                            evt.file_type, 
                            evt.path,
                            evt.type);
                        break;
                    case 'recordDone':
                        model.fileState.set(
                            connection.terminal_id,
                            channelId, 
                            evt.file_type, 
                            evt.path,
                            evt.type,
                            evt.size
                        );
                        break;
                    case 'uploadStart':
                        model.fileState.set(
                            connection.terminal_id,
                            channelId, 
                            evt.file_type, 
                            evt.path,
                            evt.type
                        );
                        break;
                    case 'uploadDone':
                        model.fileState.set(
                            connection.terminal_id,
                            channelId, 
                            evt.file_type, 
                            evt.path,
                            evt.type
                        );
                        break;
                    default:
                        break;
                }

                const subscribers = model.sequelize.subscriptions.filter( _ => _.event === evt.type);

                subscribers.forEach(element => {
                    const mailer = {server: 'mail.tintekko.com', port: 25, user: 'support@soltarget.ru', password: 'NlomKvtA15', sender: 'Attestat Sysyems' };

                    try {
                        var transporter = nodemailer.createTransport('smtp://mail.tintekko.com');
                        var mailOptions = {
                            host: mailer.serever,
                            port: mailer.port,
                            secure: false,
                            ignoreTLS: false,
                            requireTLS: false,
                            auth: {
                                user : mailer.user,
                                pass: mailer.password
                            },
                            tls:{
                                rejectUnauthorized: false
                            },                            
                            from: mailer.sender, // sender address
                            to: element.peer, // list of receivers
                            subject: 'События Attestat Systems', // Subject line
                            html: '<p>Hello! You are subscribed for this event from attestat.systems:</p><p>' + JSON.stringify(evt) + '</p>'
                        };

                        // send mail with defined transport object
                        transporter.sendMail(mailOptions, function(error, info){
                            if(error){
                                return console.log(error);
                            }
                            console.log('Message sent: ' + info.response);
                        });
                    } catch(error) {
                        console.error();
                    }
                });
            }
        }

        try {
            args.terminal_id = connection.terminal_id;
            if(api.subscriptions[connection.terminal_id]) {
                for(let i = 0; i < api.subscriptions[connection.terminal_id].length; i++) {
                    try {
                        api.call(api.subscriptions[connection.terminal_id][i], "control", "terminalEvent", args);
                    } catch(err) {}
                }
            }
        } catch(err) {}
        
        api.answer(connection, id, {result: 'ok'});
    }

    api.interface['control']['terminalInfo'] = async(connection, id, args) => {
        for(let el in args) {
            console.log(el);

            if(el === 'channels') {
                const originalChannels = await model.terminalChannel.getListByTerminal(connection.terminal_id);

                for(let i = 0; i < args.channels.length; i++) {
                    const idx = originalChannels.findIndex(x => x.serial == args.channels[i].serial);

                    let channel;
                    if(idx === -1) channel = await model.terminalChannel.add(null, connection.terminal_id, args.channels[i].serial, '', '');
                    else channel = originalChannels[idx];

                    for(let cel in args.channels[i]) {
                        if(el == 'lowThreshold') {
                            console.log(cel);
                        }
                                const vid = model.sequelize.VALUE_NAMES.indexOf(cel);
                        if(vid == -1) continue;
        
                        await model.terminalData.add(connection.terminal_id, channel.id, vid, new Date(), args.channels[i][cel], model.sequelize.KEEP_HISTORY_FOR.indexOf(cel) != -1);
                    }
                }
            } else {
                const vid = model.sequelize.VALUE_NAMES.indexOf(el);
                if(vid == -1) continue;

                await model.terminalData.add(connection.terminal_id, null, vid, new Date(), args[el], model.sequelize.KEEP_HISTORY_FOR.indexOf(el) != -1);
            }
        }

        try {
            args.terminal_id = connection.terminal_id;
            if(api.subscriptions[connection.terminal_id]) {
                for(let i = 0; i < api.subscriptions[connection.terminal_id].length; i++) {
                    try {
                        api.call(api.subscriptions[connection.terminal_id][i], "control", "terminalInfo", args);
                    } catch(err) {
                        console.log('error: ' + err);
                    }
                }
            }
        } catch(err) {
            console.log('error: ' + err);
        }

        api.answer(connection, id, {result: 'ok'});
    }

    api.interface['control']['subscribe'] = async(connection, id, args) => {
        if(!api.subscriptions[args.terminal_id]) api.subscriptions[args.terminal_id] = [];

        api.subscriptions[args.terminal_id].push(connection);

        api.answer(connection, id, {result: 'ok'});
    }

    api.interface['control']['unsubscribe'] = async(connection, id, args) => {
        if(!api.subscriptions[args.terminal_id]) return;

        const idx = api.subscriptions[args.terminal_id].findIndex(cc => cc.ws_connection_id == connection.ws_connection_id);
        api.subscriptions[args.terminal_id].splice(idx, 1);

        api.answer(connection, id, {result: 'ok'});
    }

    api.interface['control']['unsubscribeAll'] = async(connection, id, args) => {
        
        for(let ss in api.subscriptions) {
            const idx = api.subscriptions[ss].findIndex(cc => cc.ws_connection_id == connection.ws_connection_id);

            if(idx != -1) {
                if(api.subscriptions[ss][idx].stream) {
                    try {
                        await api.callAsync(api.terminalConnections[ss], 'control', 'unsubscribeStream', {stream: api.subscriptions[ss][idx].stream});
                    } catch(err) {}
                }
                api.subscriptions[ss].splice(idx, 1);
            }
        }

        if(id) api.answer(connection, id, {result: 'ok'});
    }

    api.interface['control']['subscribeStream'] = async(connection, id, args) => {
        if(!api.terminalConnections[args.terminal_id]) throw new Error("Нет соединения с терминалом");

        if(!api.subscriptions[args.terminal_id]) api.subscriptions[args.terminal_id] = [];

        const result = await api.callAsync(api.terminalConnections[args.terminal_id], 'control', 'subscribeStream', args.stream);
        if(result) {
            connection.stream = args.stream;
            api.subscriptions[args.terminal_id].push(connection);
        }

        api.answer(connection, id, result);
    }

    api.interface['control']['unsubscribeStream'] = async(connection, id, args) => {
        
        if(connection.stream) delete connection.stream;

        if(!api.terminalConnections[args.terminal_id]) throw new Error("Нет соединения с терминалом");
        
        await api.callAsync(api.terminalConnections[args.terminal_id], 'control', 'unsubscribeStream', {stream: connection.stream});

        api.answer(connection, id, {result: 'ok'});
    }

    api.interface['control']['resetCounters'] = async(connection, id, args) => {
        if(!api.terminalConnections[args.terminal_id]) throw new Error("Нет соединения с терминалом");

        const result = await api.callAsync(api.terminalConnections[args.terminal_id], 'control', 'resetCounters', args);
        api.answer(connection, id, result);
    }

    api.interface['control']['streamData'] = async (connection, packet) => {
        const buffer = new Uint8Array(packet);
        const index = buffer[0];
        const type = buffer[1];

        try {
            if(api.subscriptions[connection.terminal_id]) {
                for(let i = 0; i < api.subscriptions[connection.terminal_id].length; i++) {
                    try {
                        if(api.subscriptions[connection.terminal_id][i].stream){
                            if(api.subscriptions[connection.terminal_id][i].stream.type == type 
                            && api.subscriptions[connection.terminal_id][i].stream.index == index) {
                                api.subscriptions[connection.terminal_id][i].send(packet);
                            }
                        }
                    } catch(err) {}
                }
            }
        } catch(err) {}
    }

    api.interface['control']['ping'] = async(connection, id, args) => {
        const vid = model.sequelize.VALUE_NAMES.indexOf('ping');
        if(vid != -1) await model.terminalData.add(connection.terminal_id, null, vid, new Date(), 'ping', model.sequelize.KEEP_HISTORY_FOR.indexOf('ping') != -1);

        try {
            const evt = {
                type: 'ping',
                time: new Date(),
                terminal_id: connection.terminal_id
            };

            if(api.subscriptions[connection.terminal_id]) {
                for(let i = 0; i < api.subscriptions[connection.terminal_id].length; i++) {
                    try {
                        api.call(api.subscriptions[connection.terminal_id][i], "control", "terminalEvent", evt);
                    } catch(err) {}
                }
            }
        } catch(err) {}
        
        api.answer(connection, id, {result: 'ok'});
    }

    api.interface['control']['readInfo'] = async(connection, id, args) => {
        if(!api.terminalConnections[args.terminal_id]) throw new Error("Нет соединения с терминалом");

        const result = await api.callAsync(api.terminalConnections[args.terminal_id], 'control', 'readInfo', args);

        api.answer(connection, id, result);
    }

    api.interface['control']['setTerminalValue'] = async(connection, id, args) => {
        if(!api.terminalConnections[args.terminal_id]) throw new Error("Нет соединения с терминалом");

        const result = await api.callAsync(api.terminalConnections[args.terminal_id], 'control', 'setTerminalValue', args);

        api.answer(connection, id, result);
    }

    api.interface['control']['setChannelValue'] = async(connection, id, args) => {
        if(!api.terminalConnections[args.terminal_id]) throw new Error("Нет соединения с терминалом");

        const result = await api.callAsync(api.terminalConnections[args.terminal_id], 'control', 'setChannelValue', args);

        api.answer(connection, id, result);
    }

    api.interface['control']['fileAnalyzeStart'] = async(connection, id, args) => {
    }

    api.interface['control']['fileAnalyzeStop'] = async(connection, id, args) => {
    }
}
