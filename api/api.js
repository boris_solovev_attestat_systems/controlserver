var WebSocketServer = require('ws').Server;
var server_config = require('../server_config');
var jwt = require('jsonwebtoken');

var api = {};

var ws = new WebSocketServer({port: server_config.ws.port});

console.log('listen for ws on ' + server_config.ws.port);

api.interface = {};
api.control = {};
api.callback = {};
api.callid = 1;
api.clientConnections = {};
api.subscriptions = {};
api.terminalConnections = {};
api.clientid = 1;

ws.on('connection', function (ws) {
    console.log('incoming connection');

    ws.ws_connection_id = api.clientid; api.clientid++;
    ws.binaryType = 'arraybuffer';

    ws.on('message', function (message) {
        try {
            if(typeof message == 'string') {
                var _message = JSON.parse(message);

                console.log('message type: ' + _message.type);

                switch(_message.type) {
                    case 'error':
                    case 'answer': {
                            if (_message.id in api.callback) {
                                const callback = api.callback[_message.id];
                                delete api.callback[_message.id];
                                if (_message.type === 'error') {
                                    if ('onerror' in callback) {
                                        callback.onerror(_message.args);
                                    } else {
                                        console.error('No callback for error ' + JSON.stringify(_message));
                                    }
                                } else {
                                    callback(_message.args);
                                    //console.log('good');
                                }
                            } else {
                                console.log(`Unknown callback:` + JSON.stringify(_message));
                            }
                        }
                        break;

                    case 'query': {
                            const core = async() => {
                                if(_message.interface in api.interface) {
                                    if(_message.method in api.interface[_message.interface]) {
                                        try {
                                            _error = await api.control[_message.interface][_message.method](ws, _message.id, _message.args);

                                            if(_error != null) api.error(ws, _message.id, {error: _error});
                                            else await api.interface[_message.interface][_message.method](ws, _message.id, _message.args);
                                        } catch (e){
                                            api.error(ws, _message.id, {error: e.message + ' ' + _message.interface + '.' + _message.method});
                                        }
                                    } else {
                                        api.error(ws, _message.id, {error: 'no method ' + _message.interface + '.' + _message.method});    
                                    }
                                } else {
                                    api.error(ws, _message.id, {error: 'no interface ' + _message.interface});
                                }
                            }

                            core().catch((err) => {
                                api.error(ws, 0, {error: err.message});  
                            });
                        }
                        break;
                    default:
                        break;
                }
            } else {
                api.interface.control.streamData(ws, message);
            }
        } catch(e) {
            console.error('Message error: ' + JSON.stringify(e));
        }
    });

    ws.on('error', () => {
        console.log('errored...');
        if(ws.ws_connection_id) {
            api.interface["control"]["unsubscribeAll"](ws, null, null).then(() => {
                if(ws.terminal_id)  delete api.terminalConnections[ws.terminal_id];
                else delete api.clientConnections[ws.ws_connection_id];
            });
        }
        console.log('errored');
    });

    ws.on('closed', () => {
        console.log('closed...');
        if(ws.ws_connection_id) {
            api.interface["control"]["unsubscribeAll"](ws, null, null).then(() => {
                if(ws.terminal_id)  delete api.terminalConnections[ws.terminal_id];
                else delete api.clientConnections[ws.ws_connection_id];
            });
        }
        console.log('closed');
    });
});

api.call = function(_ws, _interface, _method, _args, _onresult, _onerror, broadcast) {
    try {
        var _id = api.callid++;
        if(_onresult != undefined) api.callback[_id] = _onresult;
        if(_onerror != undefined) api.callback[_id]['onerror'] = _onerror;
        _ws.send(JSON.stringify({type: 'query', interface: _interface, method: _method, id: _id, args: _args}));
        if(broadcast && _onresult) _onresult;
    }
    catch(e) {
        //console.error();
    }
}

api.callAsync = function(_ws, _interface, _method, _args) {
    return new Promise((resolve, reject) => {
        api.call(_ws, _interface, _method, _args,
        (args) => {
            resolve(args);
        },
        (args) => {
            reject(args);
        });
    });
}

api.answer = function(_ws, _id, _args) {
    console.log('answer');
    _ws.send(JSON.stringify({type: 'answer', id: _id, args: _args}));
}

api.error = function(_ws, _id, _args) {
    console.log('error');
    _ws.send(JSON.stringify({type: 'error', id: _id, args: _args}));
}

module.exports = api;