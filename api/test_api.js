module.exports = function(model, api) {
    api.interface['test'] = {};
    api.control['test'] = {};

//
//                          Control
//
    async function everyOne(connection, id, args) {
        return null;
    }

    api.control['test']['startNewTask'] = everyOne;
    api.control['test']['completeTask'] = everyOne;

//
//                          API
//

    api.interface['test']['startNewTask'] = async(connection, id, args) => {
        let task_id = null;
        
        try {
            task_id = (await model.sequelize.query("INSERT INTO public.file_test_task(person, start_time, file_state_id) \
            VALUES ( \
                '" + args.person + "', \
                current_timestamp, \
                (SELECT id FROM file_state WHERE state='uploadDone' AND type='d' AND NOT EXISTS (SELECT id FROM file_test_task WHERE file_state_id=file_state.id) ORDER BY \"createTime\" LIMIT 1) \
            );SELECT currval('\"file_test_task_id_seq\"'::regclass);", {type: model.sequelize.QueryTypes.SELECT}))[0].currval;
        } catch(error) {console.log(error);};

        if(!task_id) return api.error(connection, id, {error: 'Новых задач нет'});

        const test_task = (await model.sequelize.query("SELECT * FROM file_test_task WHERE id=" + task_id, {type: model.sequelize.QueryTypes.SELECT}))[0];
        const file_state = await model.fileState.getById(test_task.file_state_id);

        api.answer(connection, id, {test_task, file_state});
    }

    api.interface['test']['completeTask'] = async(connection, id, args) => {
        let in_count = 0;
        let out_count = 0;

        for(let i = 0; i < args.frames.length; i++) {
            const f = args.frames[i];
            in_count += f.in_count;
            out_count += f.out_count;
            await model.sequelize.query('INSERT INTO public.file_test_task_frame(frame, in_count, out_count, task_id) VALUES (' + f.number + ', ' + f.in_count + ', ' + f.out_count + ', ' + args.task_id + ');');
        };

        const test_task = (await model.sequelize.query("UPDATE public.file_test_task SET end_time=current_timestamp WHERE id=" + args.task_id));

        api.answer(connection, id, {result: 'ok'});
    }

}
