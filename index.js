var server_config = require('./server_config');
var model = require('./model/model');
var api = require('./api/api');
var http_server = require('./http_server');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var basic_api = require('./api/basic_api')(model, api);
var vehicle_api = require('./api/vehicle_api')(model, api);
var terminal_api = require('./api/terminal_api')(model, api);
var control_api = require('./api/control_api')(model, api);

var test_api = require('./api/test_api')(model, api);

api.interface['basic'].checkUser = model.user.checkUser;
api.interface['basic'].checkTerminal = model.terminal.checkTerminal;

console.log("Server started");